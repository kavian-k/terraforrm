#!/bin/bash

#kubectl delete namespace $1 --kubeconfig ~/.kube/config
#kubectl create namespace $1 --kubeconfig ~/.kube/config

kubectl get namespaces --kubeconfig ~/.kube/config | grep $1
RESULT=$?
if [ $RESULT -eq 0 ]; then
  kubectl delete namespace $1 --kubeconfig ~/.kube/config
fi