variable "namespace" {}


provider "kubernetes" {
  config_path  = "~/.kube/config"
}

provider "helm" {
  kubernetes {
    config_path = "~/.kube/config"
  }
}

resource "null_resource" "deleteNS" {
  provisioner "local-exec" {
    command = "./ns.sh ${var.namespace}"
    interpreter = ["/bin/bash","-c"]
  }
}

resource "kubernetes_namespace" "ns" {
  metadata {
    name = var.namespace
  }
  depends_on = [
      null_resource.deleteNS
    ]
}

resource "helm_release" "nginx" {
  name       = "my-nginx"
  repository = "https://charts.bitnami.com/bitnami"
  chart      = "nginx"
  namespace  = var.namespace
  wait = false
  depends_on = [
    kubernetes_namespace.ns
  ]
}
